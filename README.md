##thinkphp6.0+thinkphp-swoole3的websocket实现demo

> 运行环境要求PHP7.1+

> swoole4.5+

> 测试地址: `https://域名/static/web/ws.html`

> wss连接地址: `wss://域名/ws?token=3` token留作鉴权使用,自行实现.demo会将token作为unique使用

> 命令格式: 
- 全`methods`执行`{"cmd":"sys"}`
- 单/多`methods`执行`{"cmd":"sys.ping"}` / `{"cmd":"sys.ping.syncMsg"}`

> 实现了配置化task任务,参照demo样例配置实现即可.



> _**注意:在服务启动时(SwooleBoot.php)会对task.php与websocket.php的class/methods进行检查!若检查失败则停止启动!**_

> go版本实现地址 https://gitee.com/tsbjt/goframe-websocket

> nginx配置如下
~~~
server {
	listen       443;
	
	ssl on;
	ssl_certificate 证书.pem;
	ssl_certificate_key 证书.key;
	ssl_session_timeout 5m;
	ssl_ciphers ECDHE-RSA-AES128-GCM-SHA256:ECDHE:ECDH:AES:HIGH:!NULL:!aNULL:!MD5:!ADH:!RC4;
	ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
	ssl_prefer_server_ciphers on;
        server_name  域名;
	location / {
		proxy_http_version 1.1;
        proxy_set_header Connection "keep-alive";
        proxy_set_header X-Real-IP $remote_addr;
        if (!-e $request_filename) {
             proxy_pass http://127.0.0.1:9501;
        }
	}
        location /ws {	
		proxy_connect_timeout 60;
        proxy_send_timeout 60;
        proxy_read_timeout 60;
        proxy_buffer_size 256k;
        proxy_buffers 4 256k;
        proxy_busy_buffers_size 256k;
        proxy_temp_file_write_size 256k;
        proxy_next_upstream error timeout invalid_header http_500 http_502 http_503 http_404;
        proxy_max_temp_file_size 128m;
        proxy_pass http://127.0.0.1:9501;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";	
	 }
}

~~~