<?php

namespace app\controller;

use app\BaseController;
use Swoole\Server;

class Index extends BaseController
{
    public function index()
    {
        echo 'think-swoole websocket demo.';
    }

    public function doTask(Server $server)
    {
        echo getmypid().PHP_EOL;
        echo $memory = round(memory_get_usage() / 1024 / 1024, 2).'MiB___'.PHP_EOL;
        $server->task(['cmd'=>'lottery']);
    }
}
