<?php
declare (strict_types=1);

namespace app\service\websocket;


use Swoole\WebSocket\Server;

class Sys
{

    public static function ping($data, $unique, Server $server, $fd)
    {
        $server->push($fd, "pong");
        return ["ping" => "pong"];
    }

    public static function syncMsg($data, $unique, Server $server, $fd)
    {
        return ["syncMsg" =>"syncMsgCall"];
    }
}