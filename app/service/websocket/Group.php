<?php
declare (strict_types=1);

namespace app\service\websocket;


use Swoole\WebSocket\Server;

class Group
{
    public static function add($data, $unique, Server $server, $fd)
    {
        $sum = 0;
        for ($i = 0; $i < 1000; $i++) {
            $sum += $i;
        }
        return ["group" => "add" . $sum];
    }

    public static function quit($data, $unique, Server $server, $fd)
    {
        $sum = 0;
        for ($i = 0; $i < 2000; $i++) {
            $sum += $i;
        }
        return ["group" => "quit". $sum];
    }

    public static function sponsor($data, $unique, Server $server, $fd)
    {
        $sum = 0;
        for ($i = 0; $i < 3000; $i++) {
            $sum += $i;
        }
        return ["group" => "sponsor". $sum];
    }
}